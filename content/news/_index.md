---
title: "news"
date: 2023-08-14T17:51:47+03:30
draft: false
headless: true

# all icons by [feathericons.com](https://https://feathericons.com//) are supported
show_news_icons: true
default_news_icon: "file-text"

num_news: 3

news_items:
- text: "I joined Statify Team at Inria Grenoble as a Postdoc Researcher"
  extra_text: "September 2023."
  date: 2023-09-01
- text: "I successfully defended my Ph.D. thesis"
  extra_text: "June 2023."
  date: 2023-06-05
- text: "I joined Kings College London as Visiting Student Research"
  extra_text: "July 2022."
  date: 2022-07-04
---
